'use strict';

/**
 * @ngdoc service
 * @name nrlApp.canvasimageservice
 * @description
 * # canvasimageservice
 * Service in the nrlApp.
 */
angular.module('nrlApp')
  .service('canvasImageService', function($http) {

    var LOGO_URL = "http://"+url+"/images/nrl-logo-70.png";
    var POST_API_URL = "http://"+url+"/api/Prediction";

    var WIDTH = 1080;
    var HEIGHT = 1080;
    var PICTURE_Y = 490;
    var LOGO_Y = 710;
    var GREY_LINE_WIDTH = 372;
    var GREY_LINE_Y = 900;
    var SCORE_X = 180;
    var SCORE_Y = 910;
    var TEXT_1_Y = 945;
    var TEXT_1_X = 280;
    var TEXT_2_Y = 1000;
    var TEXT_2_X = 800;
    var TEXT_3_Y = 1056;
    var TEAM_Y = 970;

    var CanvasImage = function (canvas, team1, team2, bgImageUrl, winningImageUrl, losingImageUrl, picture, score1, score2, cb) {

      this.cb = cb;
      this.canvas = canvas;
      this.context = canvas.getContext('2d');
      this.itemsCount = 5;
      this.itemsLoaded = 0;
      this.dataURL = null;

      this.team1 = team1.toUpperCase();
      this.team2 = team2.toUpperCase();
      this.bgImageUrl = bgImageUrl;
      this.winningImageUrl = winningImageUrl;
      this.losingImageUrl = losingImageUrl;
      this.pictureImageUrl = picture;
      this.score1 = score1;
      this.score2 = score2;

      this.imageWinning = new Image();
      this.imageLosing = new Image();
      this.imageLogo = new Image();
      this.imageBg = new Image();
      this.imagePic = new Image();

      this.imageWinning.crossOrigin = "Anonymous";
      this.imageLosing.crossOrigin = "Anonymous";
      this.imageLogo.crossOrigin = "Anonymous";
      this.imageBg.crossOrigin = "Anonymous";
      this.imagePic.crossOrigin = "Anonymous";
    };

    CanvasImage.prototype = {

      constructor: CanvasImage,

      load: function() {
        this.imageWinning.onload = function() {
          this.itemsLoaded++;
          this.checkFinish();
        }.bind(this);
        this.imageBg.onload = function() {
          this.itemsLoaded++;
          this.checkFinish();
        }.bind(this);
        this.imageBg.src = this.bgImageUrl;
        this.imageWinning.src = this.winningImageUrl;
        this.imageLosing.onload = function() {
          this.itemsLoaded++;
          this.checkFinish();
        }.bind(this);
        this.imageLosing.src = this.losingImageUrl;
        this.imageLogo.onload = function() {
          this.itemsLoaded++;
          this.checkFinish();
        }.bind(this);
        this.imageLogo.src = LOGO_URL;

        if ( this.pictureImageUrl ) {
          this.imagePic.onload = function() {
            this.itemsLoaded++;
            this.checkFinish();
          }.bind(this);
          this.imagePic.src = this.pictureImageUrl;
        } else {
          this.itemsLoaded++;
          this.checkFinish();
        }
      },

      checkFinish: function() {
        if (this.itemsLoaded === this.itemsCount) {
          this.draw();
          this.createDataURL();
          this.sendImageToApi();
        }
      },

      draw: function() {
        var text = null;
        var textMetrics = null;
        var context = this.context;
        context.save();

        // Draw images
        context.drawImage(this.imageBg, 0, 0);
        context.drawImage(this.imageWinning, 0, 0);
        context.drawImage(this.imageLosing, 0, 0);
        
        if ( this.pictureImageUrl ) {
          // Draw profile pic in circle
          var w = this.imagePic.width;
          var h = this.imagePic.height;
          // white border
          context.beginPath();
          context.arc(WIDTH/2,h/2+PICTURE_Y,h/2+6,0,Math.PI*2);
          context.fillStyle = "#fff";
          context.fill();
          context.closePath();
          context.clip();
          // image
          context.beginPath();
          context.arc(WIDTH/2,h/2+PICTURE_Y,h/2,0,Math.PI*2);
          context.closePath();
          context.clip();
          context.drawImage(this.imagePic, WIDTH/2-w/2, PICTURE_Y);
          context.restore();
        }

        // Logo
        context.drawImage(this.imageLogo, (WIDTH-this.imageLogo.width)/2, LOGO_Y);
        
        // Draw grey line
        // context.beginPath();
        // context.moveTo((WIDTH-GREY_LINE_WIDTH)/2, GREY_LINE_Y);
        // context.lineTo((WIDTH-GREY_LINE_WIDTH)/2+GREY_LINE_WIDTH, GREY_LINE_Y);
        // context.strokeStyle = 'rgba(63,64,66,0.8)';
        // context.stroke();

        // Draw bottom text
        text = "NRL.COM/NRLGF";
        context.font = "44px rlfont";
        context.fillStyle = "#f0db00";
        textMetrics = context.measureText(text);
        context.fillText(text, TEXT_2_X-(textMetrics.width/2), TEXT_3_Y);

        text = "MY CALL FOR THE";
        context.font = "42px rlfont";
        context.fillStyle = "#a4a4a4";
        textMetrics = context.measureText(text);
        context.fillText(text, TEXT_1_X-(textMetrics.width/2), TEXT_3_Y);

        text = "#NRLGF";
        context.font = "42px rlfont";
        context.fillStyle = "#a4a4a4";
        textMetrics = context.measureText(text);
        context.fillText(text, (WIDTH-textMetrics.width)/2, TEXT_3_Y);

        // Draw team scores
        text = this.score1;
        context.font = "bold 220px rlfont";
        context.fillStyle = "#fff";
        textMetrics = context.measureText(text);
        context.fillText(text, SCORE_X-(textMetrics.width/2), SCORE_Y);
        text = this.score2;
        textMetrics = context.measureText(text);
        context.fillText(text, (WIDTH-SCORE_X)-(textMetrics.width/2), SCORE_Y);

        text = this.team1;
        context.font = "bold 44px rlfont";
        context.fillStyle = "#f0db00";
        textMetrics = context.measureText(text);
        context.fillText(text, SCORE_X-(textMetrics.width/2), TEAM_Y);
        text = this.team2;
        textMetrics = context.measureText(text);
        context.fillText(text, (WIDTH-SCORE_X)-(textMetrics.width/2), TEAM_Y);
      },

      createDataURL: function() {
        this.dataURL = this.canvas.toDataURL("image/jpeg");
      },

      sendImageToApi: function() {
        var data = this.dataURL.replace("data:image/jpeg;base64,", "");
        var predictionObj = { ImageData: data };

        // $http.post(POST_API_URL, predictionObj, {withCredentials:true}).
        //   then(function(response) {
        //     console.log(response);
        //   }, function(response) {
        //     // error
        //     console.log(response);
        //   });
        window.$.ajax({
            type: "POST",
            url: POST_API_URL,
            data: predictionObj
        }).done(function (response) {
            console.log(response);
            this.cb(response);
        }.bind(this));
      }
    };

    CanvasImage.generateImage = function(canvas, team1, team2, bgImageUrl, winningImageUrl, losingImageUrl, picture, score1, score2, cb) {
      var canvasImage = new CanvasImage(canvas, team1, team2, bgImageUrl, winningImageUrl, losingImageUrl, picture, score1, score2, cb);
      return (canvasImage.load());
    };

    return CanvasImage;

  });

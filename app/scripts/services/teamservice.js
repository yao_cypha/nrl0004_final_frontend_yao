'use strict';

/**
 * @ngdoc service
 * @name nrlApp.teamService
 * @description
 * # teamImageService
 * Service in the nrlApp.
 */
angular.module('nrlApp')
  .service('teamService', ['$http', function ($http) {

    return {
      getTeams: function() {
        return $http.get('api/teams_v1.json').then(function(result) {
          return result.data;
        });
      }
    };

  }]);

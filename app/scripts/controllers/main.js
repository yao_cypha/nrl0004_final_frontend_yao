'use strict';

var url = "nrlfinalstage.azurewebsites.net";
//var url = "nrlgf.com.au";

var redirect_uri = "http://"+url;
var appId = '1676650662578668';

/**
 * @ngdoc function
 * @name nrlApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nrlApp
 */
angular.module('nrlApp')
  .controller('MainCtrl', ['$scope', '$rootScope', '$timeout', 'WizardHandler', 'teamService', 'preloaderService', 'canvasImageService',
    function ($scope, $rootScope, $timeout, WizardHandler, teamService, preloaderService, canvasImageService) {

    var SHARE_URL = "http://"+url+"/share.php?filename=";
    var SHARE_TEXT = "I've just made a call for the 2015 Telstra Premiership Grand Final. Make your call at nrl.com/nrlgf #nrlgf";
    var PICTURE_PLACEHOLDER = "http://"+url+"/images/picture_placeholder.jpg";

    $scope.allTeams = [];
    $scope.teams = [];
    $scope.teams2015 = [];
    $scope.teams2016 = [];
    $scope.teamsLosing = [];
    $scope.team1 = null;
    $scope.team2 = null;
    $scope.year = 2015;
    $scope.teamImagesLocations = [];
    $scope.activeWinningImage = null;
    $scope.activeLosingImage = null;
    $scope.result = {
      score1: '',
      score2: 0
    };
    $scope.currentStepName = '';
    $scope.user = {};

    $scope.isLoading = true;
    $scope.sharingImageUrl = null;

    teamService.getTeams().then(function(data) {
      $scope.allTeams = data;
      $scope.teamImagesLocations = window._.pluck($scope.allTeams, 'image_url_logo');
      setUpTeams();
      setTeams();
      loadTeamImages();
    });

    // WIZARD STEPS
    $scope.teamInFinal = function() {
      $scope.year = 2015;
      setTeams();      

      if (document.URL.indexOf('code') != -1) {
        // manualy logged in
        FB.getLoginStatus(function(response) {
          console.log(response);
          getFbUser();
        }, true);
      } else if (document.URL.indexOf('error') != -1) {
        // manualy logged error
        start();
      } else {
        // not logged yet
        loginUser();
      }
    };

    $scope.teamNotInFinal = function() {
      $scope.year = 2016;
      setTeams();
      WizardHandler.wizard().goTo('step1-2016-select-the-winning-team');
      createCarousel('#team-carousel-2016');
    };

    $scope.winningTeamSelected = function() {
      $scope.setUpWinningImage();
      WizardHandler.wizard().goTo('step2-'+$scope.year+'-select-the-winning-image');
      createCarousel('#image-carousel-'+$scope.year);
      if ($scope.year === 2015) {
        $scope.setUpLosingTeams();
      }
    };
    $scope.previous = function(){
      //console.log(123);
    }

    $scope.winningImageSelected = function() {
      if ($scope.year === 2015) {
        WizardHandler.wizard().goTo('step3-2015-select-the-losing-team');
        createCarousel('#losing-team-carousel');
      } else {
        WizardHandler.wizard().finish();
      }
    };

    $scope.losingTeamSelected = function() {
      $scope.setUpLosingImage();
      WizardHandler.wizard().goTo('step4-2015-select-the-losing-image');
      createCarousel('#losing-image-carousel');
    };

    $scope.losingImageSelected = function() {
      WizardHandler.wizard().goTo('step5-2015-call-the-final-score');
      $timeout(function(){ document.getElementById('team1-score').focus(); });
    };

    $scope.scoreSelected = function() {
      if ($scope.scoreValidation()) {
        WizardHandler.wizard().finish();
      } else {
        return;
      }
    };
    // END STEPS

    // WIZARD HANDLERS
    $scope.finishedWizard = function() {
      var sharingCallback = function(response) {
        $rootScope.$apply(function() {
          $scope.isLoading = false;
          $scope.sharingImageUrl = response.ImageUrl;
          console.log($scope.sharingImageUrl);
          // var link = document.getElementById('download-image');
          // link.download = $scope.team1+'-vs-'+$scope.team2+'-2015.png';
          // link.href = $scope.sharingImageUrl;
        });
      };

      if ($scope.year === 2015) {
        canvasImageService.generateImage(
          document.getElementById('image-canvas'),
          $scope.team1.name,
          $scope.team2.name,
          $scope.team1.image_url_bg,
          $scope.activeWinningImage.full_url,
          $scope.activeLosingImage.full_url,
          $scope.user.picture,
          $scope.result.score1,
          $scope.result.score2,
          sharingCallback
        );
      } else {
        $scope.sharingImageUrl = $scope.activeWinningImage.full_url;
      }
      WizardHandler.wizard().goTo('share-'+$scope.year);
    };

    $scope.scoreValidation = function(){
      return ( angular.isNumber($scope.result.score1) &&
        angular.isNumber($scope.result.score2) &&
        ($scope.result.score1 >= $scope.result.score2)
      );
    };
    // END WIZARD

    $scope.trackDownload = function ($event) {
      ga('send', 'event', 'button', 'action', 'download');
      console.log('track download');
      window.open($scope.sharingImageUrl);

      if ( $scope.year === 2015 ) {
        goToCollectData();
      }
    };

    $scope.isActiveWinningTeam = function(team) {
      return team.id === $scope.team1.id;
    };

    $scope.isActiveLosingTeam = function(team) {
      return team.id === $scope.team2.id;
    };

    $scope.setActiveWinningTeam = function(team) {
      $scope.team1 = team;
    };

    $scope.setActiveLosingTeam = function(team) {
      $scope.team2 = team;
    };

    $scope.isActiveWinningImage = function(image) {
      return $scope.activeWinningImage.id === image.id;
    };

    $scope.isActiveLosingImage = function(image) {
      return $scope.activeLosingImage.id === image.id;
    };

    $scope.setActiveWinningImage = function(image) {
      $scope.activeWinningImage = image;
    };

    $scope.setActiveLosingImage = function(image) {
      $scope.activeLosingImage = image;
    };

    $scope.setUpLosingTeams = function() {
      // $scope.teamsLosing = window._.reject($scope.teams, { id: $scope.team1.id });
      $scope.teamsLosing = $scope.teams;
      if ($scope.team1.id === $scope.teamsLosing[0].id) {
        $scope.team2 = $scope.teamsLosing[1];
      } else {
        $scope.team2 = $scope.teamsLosing[0];
      }
    };

    $scope.setUpWinningImage = function() {
      $scope.activeWinningImage = $scope.team1.winning_images[0];
    };

    $scope.setUpLosingImage = function() {
      $scope.activeLosingImage = $scope.team2.losing_images[0];
    };

    $scope.downloadImage2016 = function($event) {
      $event.currentTarget.href = $scope.team1.image_url_bg;
      $event.currentTarget.download = $scope.team1.name + '-2016.jpg';
    };

    $scope.shareFb = function() {
      var longUrl = SHARE_URL + $scope.sharingImageUrl;

      var title = "Facebook share";

      var width = 400;
      var height = 300;
      var leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
      var topPosition = (window.screen.height / 2) - ((height / 2) + 50);
      var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";

      ga('send', 'event', 'button', 'action', 'facebook_share');

      var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";

      window.open("http://www.facebook.com/sharer.php?&u="+encodeURIComponent(longUrl)+'&t='+encodeURIComponent(title), 'sharer', windowFeatures);

      if ( $scope.year === 2015 ) {
        goToCollectData();
      }

      //makeShortUrl(longUrl, function(url) {
      //  var title = "Facebook share";
      //
      //  var width = 400;
      //  var height = 300;
      //  var leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
      //  var topPosition = (window.screen.height / 2) - ((height / 2) + 50);
      //  var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
      //
      //  ga('send', 'event', 'button', 'action', 'facebook_share');
      //
      //})
    };

    $scope.shareTwitter = function() {
      var longUrl = SHARE_URL + $scope.sharingImageUrl;
      var text = SHARE_TEXT;

      var width = 400;
      var height = 300;
      var leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
      var topPosition = (window.screen.height / 2) - ((height / 2) + 50);
      var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";

      ga('send', 'event', 'button', 'action', 'twitter_share');

      window.open("http://twitter.com/share?url="+encodeURIComponent(longUrl)+"&text="+encodeURIComponent(text), 'sharer', windowFeatures);

      if ( $scope.year === 2015 ) {
        goToCollectData();
      }

      //makeShortUrl(longUrl, function(url) {
      //})
    };

    $scope.sendData = function() {
      if ($('#postcode').val()) {
        $scope.user.postcode = $('#postcode').val();
      } else {
        $scope.user.postcode = '0';
      }

      window.$.ajax({
          type: "POST",
          url: "http://"+url+"/api/supporter",
          data: {
            Email: $scope.user.email || "",
            TeamSupported: $scope.team1.name,
            Postcode: $scope.user.postcode
          }
      }).complete(function (response) {
          console.log(response);
          document.getElementById('send-data-btn').innerHTML = '<span>Thank You</span>';
          document.getElementById('send-data-btn').setAttribute("disabled", "disabled");
      }.bind(this));
    };

    var start = function() {
      WizardHandler.wizard().goTo('step1-2015-select-the-winning-team');
      createCarousel('#team-carousel');
    };

    var loginUser = function() {
      if ( navigator.userAgent.indexOf("CriOS") != -1 ) {

        // chrome on ios
        var url = 'https://www.facebook.com/dialog/oauth?client_id='+appId+'&redirect_uri='+redirect_uri+'&scope=public_profile,email';
        window.open(url, '_self');

      } else {

        window.FB.login(function(response) {
          console.log(response);
          if (response.status === 'connected') {
            getFbUser();
          } else {
            // $scope.user.picture = PICTURE_PLACEHOLDER;
            start();
          }
        }.bind(this), {scope: 'public_profile,email'});
      }
    };

    var getFbUser = function() {
      FB.api('/me', function(response) {
        console.log(response);
        $scope.user.id = response.id;
        $scope.user.email = response.email;
        $scope.user.picture = response.picture.data.url;
        start();
      }.bind(this), {fields: 'id,email,picture.width(320).height(320)'});
    };

    var goToCollectData = function() {
      $scope.year = 2016;
      $scope.teams = $scope.allTeams;
      $scope.team1 = $scope.teams[0];
      $scope.setUpWinningImage();
      console.log($scope.teams);
      WizardHandler.wizard().goTo('collect-data');
      createCarousel('#collect-data-carousel');
    };

    var makeShortUrl = function(url, cb) {
      gapi.client.urlshortener.url.insert({
        'resource': {
          'longUrl': url
        }
      }).execute(function(response) {
        if (response.id) {
          cb(response.id);
        } else {
          console.log("urlshortener error");
          cb(url);
        }
      });
    };

    var setUpTeams = function() {
      $scope.teams2015 = window._.filter($scope.allTeams, { year: 2015 });
      $scope.teams2016 = window._.filter($scope.allTeams, { year: 2016 });
    };

    var setTeams = function() {
      $scope.teams = $scope['teams'+$scope.year];
      $scope.team1 = $scope.teams[0];
      $scope.setUpWinningImage();
      if ($scope.year === 2015) {
        $scope.team2 = $scope.teams[1];
        $scope.setUpLosingImage();
      }
    };

    var loadTeamImages = function() {
      var images = $scope.teamImagesLocations.concat(
        $scope.teams2015[0].image_url_bg,
        $scope.teams2016[0].image_url_bg
      );
      preloaderService.preloadImages(images).then(
        function handleResolve(imageLocations) {
          // console.info('preload team images successful', imageLocations.length);
        },
        function handleReject(imageLocation) {
          // console.error('image failed', imageLocation);
        },
        function handleNotify(event) {
          // console.info('percent loaded: ', event.percent);
        }
      );
    };

    var createCarousel = function(id) {
      var $ = window.$;
      var owl = $(id).data('owlCarousel');

      // Remove old carousel if items updated
      if ( owl && ($(id).children().length > 2) ) {
        $(id).css('visibility', 'hidden');
        owl.destroy();
        // owl.destroy() has a bug, remove manually
        $(id).trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
        $(id).find('.owl-stage-outer').remove();
        console.info('destroy old carousel', id);
      }

      $timeout(function() {
        console.info('create carousel', id);
        // $(id).on('initialized.owl.carousel', function(event) {
        //   var item = event.item.index;
        //   var $this = $(this);
        //   $this.find('.owl-item').eq(item + 1).addClass('item-active');
        // });
        var owl = $(id);

        console.log(owl.find(".slide").length);
        if(owl.find(".slide").length>3){
          owl.owlCarousel({
            responsiveRefreshRate : 300,
            margin: 7,
            responsive:{
              0:{
                items:4,
                margin: 4
              },
              480: {
                items:4,
                margin: 4
              },
              768:{
                items:4
              },
              1024: {
                items:4
              },
              1200:{
                items:4
              },
              1300:{
                items:4
              },
              1400: {
                items: 4
              }
            },
            navigation:true
          });

          owl.find(".slide").parent().removeClass("no-slide");
          owl.parent().find(".owl-left").show();
          owl.parent().find(".owl-right").show();

          owl.parent().find(".owl-left").click(function(){
            owl.trigger('prev.owl.carousel');
          });
          owl.parent().find(".owl-right").click(function(){
            owl.trigger('next.owl.carousel');
          });
        }else if((owl.find(".slide").length==3)){
          owl.owlCarousel({
            responsiveRefreshRate : 300,
            margin: 7,
            center:true,
            startPosition:1,
            mouseDrag:false,
            responsive:{
              0:{
                items:4,
                margin: 4
              },
              480: {
                items:4,
                margin: 4
              },
              768:{
                items:4
              },
              1024: {
                items:4
              },
              1200:{
                items:4
              },
              1300:{
                items:4
              },
              1400: {
                items: 4
              }
            },
            navigation:true
          });


          //owl.find(".slide").parent().addClass("no-slide");
          owl.parent().find(".owl-left").hide();
          owl.parent().find(".owl-right").hide();

        }else{
          owl.owlCarousel({
            responsiveRefreshRate : 300,
            margin: 7,
            center:true,
            startPosition:1,
            mouseDrag:false,
            responsive:{
              0:{
                items:4,
                margin: 4
              },
              480: {
                items:4,
                margin: 4
              },
              768:{
                items:4
              },
              1024: {
                items:4
              },
              1200:{
                items:4
              },
              1300:{
                items:4
              },
              1400: {
                items: 4
              }
            },
            navigation:true
          });


          owl.find(".slide").parent().addClass("no-slide");
          owl.parent().find(".owl-left").hide();
          owl.parent().find(".owl-right").hide();
        }



        // $(id+' a').on('mouseenter', function(event) {
        //   $(this).addClass('itemhovered');
        // });

        // $(id+' a').on('mouseout', function(event) {
        //   $(this).removeClass('itemhovered');
        // });

        // $(id).on('changed.owl.carousel', function(event) {
        //   var item = event.item.index;
        //   var $this = $(this);
        //   $('.owl-item').removeClass('item-active');
        //   $this.find('.owl-item').eq(item + 1).addClass('item-active');
        // });

        $(id).css('visibility', 'visible');
      });
    };

  }]);

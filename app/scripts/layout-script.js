'use strict';

// Global Variables

var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);

(function($) {
  $(document).ready(function() {

    if (!isMobile) {
      // Desktop: zoom 50%
      $('.wrapper').css('transform', 'scale(0.5,0.5)');
      $('.wrapper').css('-moz-transform', 'scale(0.5,0.5)');
      $('.wrapper').css('margin-top', '-140px');
      // 
      // Dont' zoom carousel, zoom Image slightly
      var style = $("<style>"+
        ".content-box { transform:scale(1.3,1.3);-moz-transform:scale(1.3,1.3);-webkit-transform:scale(1.3,1.3);margin-bottom:130px; } "+
        ".carousel-container { transform:scale(2.0,2.0);-moz-transform:scale(2.0,2.0);-webkit-transform:scale(2.0,2.0);margin-top:100px;margin-bottom:100px;margin-left:auto;margin-right:auto; } "+
        ".intro .main-bg { transform:scale(1.5,1.5);-moz-transform:scale(1.5,1.5);-webkit-transform:scale(1.5,1.5) }"+
      "</style>");
      $('html > head').append(style);
    }

    $('input, textarea').placeholder();
    $('.icheck').iCheck({
      checkboxClass: 'icheckbox',
          radioClass: 'iradio',
          hoverClass: 'label-hover',
          labelHoverClass: 'label-hover'
    });

    window.addEventListener('load', function() {
      new window.FastClick(document.body);
    }, false);


    // $('.carousel').on('initialized.owl.carousel', function(event) {
    //  var item = event.item.index;
    //  var $this = $(this);
    //  $this.find('.owl-item').eq(item + 1).addClass('item-active');
    // });

    // $('.carousel').owlCarousel({
    //     margin: 7,
    //     responsive:{
    //         0:{
    //             items:4,
    //             margin: 4
    //         },
    //         480: {
    //          items:4,
    //          margin: 4
    //         },
    //         768:{
    //             items:4
    //         },
    //         1024: {
    //          items:4
    //         },
    //         1200:{
    //             items:4
    //         },
    //         1300:{
    //             items:4
    //         },
    //         1400: {
    //          items: 4
    //         }
    //     }
    // });


    // $('.carousel').on('changed.owl.carousel', function(event) {
    //  var item = event.item.index;
    //  var $this = $(this);
    //  $('.owl-item').removeClass('item-active');
    //  $this.find('.owl-item').eq(item + 1).addClass('item-active');
    // });

  });

})(jQuery);

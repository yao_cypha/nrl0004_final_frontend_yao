'use strict';

/**
 * @ngdoc overview
 * @name nrlApp
 * @description
 * # nrlApp
 *
 * Main module of the application.
 */
angular
  .module('nrlApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'mgo-angular-wizard',
    'ngPatternRestrict'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

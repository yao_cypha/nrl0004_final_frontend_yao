<?php
    $image = '';
    $socialImage = '';

    if (array_key_exists('filename', $_GET))
        $image = $_GET['filename'];
        $socialImage = str_replace("PredictionImages", "SocialImages", $image);
?>
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>NRL Grand Finals</title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- for Facebook -->          
        <meta property="og:title" content="NRL Grand Final Sydney" />
        <meta property="og:image" content="<?php echo $socialImage; ?>" />
        <meta property="og:url" content="<?php echo 'http://nrlgf.com.au/share.php?filename='.$socialImage; ?>" />
        <meta property="og:description" content="I've just made a call for the 2015 Telstra Premiership Grand Final. Make your call at nrl.com/nrlgf #nrlgf" />

        <!-- for Twitter -->
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@NRL" />
        <meta name="twitter:title" content="NRL Grand Final Sydney" />
        <meta name="twitter:description" content="I've just made a call for the 2015 Telstra Premiership Grand Final. Make your call at nrl.com/nrlgf #nrlgf" />
        <meta name="twitter:image" content="<?php echo $socialImage; ?>" />

        <link rel="icon" type="image/ico" href="favicon.ico" />
        <link href="stylesheets/bootstrap.css" rel="stylesheet" />
        <link href="stylesheets/style.css" rel="stylesheet" />
        <!-- Facebook Conversion Code for nrl.com/nrlgf activation click through -->
        <script>(function() {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded) {
        var fbds = document.createElement('script');
        fbds.async = true;
        fbds.src = '//connect.facebook.net/en_US/fbds.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(fbds, s);
        _fbq.loaded = true;
        }
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', '6029595768549', {'value':'0.00','currency':'AUD'}]);
        </script>
        <noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6029595768549&amp;cd[value]=0.00&amp;cd[currency]=AUD&amp;noscript=1" /></noscript>
    </head>
    <body>
        <div class="wrapper">
            <div class="content-box" style="background: url(<?php echo $image; ?>) no-repeat; background-size: cover;">
                <div class="content-box-info">
                    <div class="content">
                    </div>
                </div>
            </div>
            <h2><strong>WHAT’s YOUR</strong> <span class="text-primary">GRAND FINAL CALL?</span></h2>
            <div class="wrapper-offset">
                <div class="container-fluid">
                    <div class="btns">
                        <a href="index.html" class="btn btn-primary btn-block"><span>Create your own</span></a>
                    </div>
                </div>
            </div>
            <div class="logo"></div>
        </div>
        <div class="footer"> 
            <!-- This is for footer content -->
        </div>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>
          var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);

          $(document).ready(function() {
            if (!isMobile) {
              $('.wrapper').css('transform', 'scale(0.5,0.5)');
              $('.wrapper').css('-moz-transform', 'scale(0.5,0.5)');
              $('.wrapper').css('margin-top', '-140px');
              var style = $("<style>"+
                ".content-box { transform:scale(1.3,1.3);-moz-transform:scale(1.3,1.3);-webkit-transform:scale(1.3,1.3);margin-bottom:130px; } "+
                ".carousel-container { transform:scale(2.0,2.0);-moz-transform:scale(2.0,2.0);-webkit-transform:scale(2.0,2.0);margin-top:100px;margin-bottom:100px;margin-left:auto;margin-right:auto; } "+
              "</style>");
              $('html > head').append(style);
            }
          });
        </script>
        <script type="text/javascript">
            var initGoogleApi = function() {
                gapi.client.setApiKey('AIzaSyCA9UBlsUbaXKjSFenH6FOgATTctKIqchc');
                gapi.client.load('urlshortener', 'v1', function(){});
            };
        </script>
        <script src="https://apis.google.com/js/client.js?onload=initGoogleApi"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID -->
        <script>
          !function(A,n,g,u,l,a,r){A.GoogleAnalyticsObject=l,A[l]=A[l]||function(){
          (A[l].q=A[l].q||[]).push(arguments)},A[l].l=+new Date,a=n.createElement(g),
          r=n.getElementsByTagName(g)[0],a.src=u,r.parentNode.insertBefore(a,r)
          }(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-67430513-1');
          ga('send', 'pageview');
        </script>
    </body>
</html>

'use strict';

describe('Service: preloaderservice', function () {

  // load the service's module
  beforeEach(module('nrlApp'));

  // instantiate service
  var preloaderservice;
  beforeEach(inject(function (_preloaderservice_) {
    preloaderservice = _preloaderservice_;
  }));

  it('should do something', function () {
    expect(!!preloaderservice).toBe(true);
  });

});

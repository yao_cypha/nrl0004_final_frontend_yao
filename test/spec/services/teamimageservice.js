'use strict';

describe('Service: teamImageService', function () {

  // load the service's module
  beforeEach(module('nrlApp'));

  // instantiate service
  var teamImageService;
  beforeEach(inject(function (_teamImageService_) {
    teamImageService = _teamImageService_;
  }));

  it('should do something', function () {
    expect(!!teamImageService).toBe(true);
  });

});
